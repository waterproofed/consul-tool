// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cli

import (
	"fmt"
	"io"
	"os"

	"github.com/fatih/color"
	"github.com/hashicorp/vault/api"
	"github.com/mattn/go-colorable"
	"github.com/mitchellh/cli"
)

type RunOptions struct {
	Color   bool
	Stdout  io.Writer
	Stderr  io.Writer
	Address string
	Client  *api.Client
}

func Run(args []string) int {
	return RunCustom(args, nil)
}

func RunCustom(args []string, runOpts *RunOptions) int {
	noColor := color.NoColor

	if runOpts == nil {
		runOpts = &RunOptions{
			Stdout: os.Stdout,
			Stderr: os.Stderr,
			Color:  !noColor,
		}
	}

	if noColor {
		runOpts.Stdout = colorable.NewNonColorable(runOpts.Stdout)
		runOpts.Stderr = colorable.NewNonColorable(runOpts.Stderr)
	} else {
		if f, ok := runOpts.Stdout.(*os.File); ok {
			runOpts.Stdout = colorable.NewColorable(f)
		}
		if f, ok := runOpts.Stderr.(*os.File); ok {
			runOpts.Stderr = colorable.NewColorable(f)
		}
	}

	var ui cli.Ui = &cli.BasicUi{
		Writer:      runOpts.Stdout,
		ErrorWriter: runOpts.Stderr,
	}

	if !noColor {
		ui = &cli.ColoredUi{
			ErrorColor: cli.UiColorRed,
			WarnColor:  cli.UiColorYellow,
			Ui:         ui,
		}
	}

	command := &cli.CLI{
		Args:         args,
		Name:         "consul-tool",
		Version:      VersionText(),
		Commands:     Commands(ui, runOpts, noColor),
		Autocomplete: true,
	}

	exitCode, err := command.Run()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error executing CLI: %s\n", err.Error())
		return 1
	}

	return exitCode
}
