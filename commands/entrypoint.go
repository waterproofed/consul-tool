// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package commands

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"syscall"
	"github.com/hashicorp/vault/api"
	"github.com/pkg/errors"
	"github.com/posener/complete"
	"github.com/simon-wenmouth/consul-tool/flags"
	)

type EntrypointCommand struct {
	*BaseCommand

	// the path to the configuration files
	flagConfigurationPath  string
	flagDataPath           string

	// tls certificate provisioning mode
	flagTlsProvision       string
	flagTlsDisableServer   bool

	// if we request a certificate from a vault trusted root ca
	flagTrustedRootCaAddr  string
	flagTrustedRootCaToken string
	flagTrustedRootCaRole  string

	// client or server
	flagEntrypointMode     string

	// 00-base.json
	flagAdvertiseInterface string
	flagClientInterface    string
	flagDatacenter         string
	flagDomain             string
	flagEnableScriptChecks bool
	flagLogLevel           string
	flagPortDns            int
	flagPortHttp           int
	flagPortHttps          int
	flagPortSerfLan        int
	flagPortSerfWan        int
	flagPortServer         int
	flagRaftProtocol       int

	// 01-gossip.json
	flagGossipEncryption   bool
	flagGossipEncryptKey   string

	// 02-ssl.json
	flagRpcEncryption      bool
	flagRpcVerifyIncoming  bool
	flagRpcVerifyOutgoing  bool

	// 04-cluster.json
	flagServerName         string
	flagServerList         string

	// 05-recursors.json
	flagRecursorList       string

	// 06-telemetry.json
	flagTelemetryEnable    bool
	flagDogstatsdAddr      string
	flagDogstatsdTags      string
	flagStatsdAddr         string

	// 07-acls.json
	flagAclEnable          bool
}

func (c *EntrypointCommand) Help() string {
	helpText := `
Usage: consul-tool entrypoint [options]

      The entrypoint command is used to configure a Consul instance using
      properties supplied at the command line or via environment variables.

` + c.Flags().Help()

	return strings.TrimSpace(helpText)
}

func (c *EntrypointCommand) Synopsis() string {
	return "Docker ENTRYPOINT"
}

func (c *EntrypointCommand) Flags() *flags.FlagSets {
	set := c.flagSet(FlagSetTLS)

	f := set.NewFlagSet("Command Options")

	f.StringVar(&flags.StringVar{
		Name:    "configuration-path",
		Default: "/opt/consul/config",
		EnvVar:  "TOOL_CONFIGURATION_PATH",
		Target:  &c.flagConfigurationPath,
		Usage:   "Path on the local disk to the directory into which the" +
			"configuration files will be written.",
	})

	// tls
	f.StringVar(&flags.StringVar{
		Name:    "tls-provision",
		Default: "no",
		EnvVar:  "TOOL_TLS_PROVISION",
		Target:  &c.flagTlsProvision,
		Usage:   "The manner in which the Consul TLS certificates will be " +
			"provisioned.  Valid values include: 'vault' and 'no'. " +
			"The 'vault' method makes a request to a vault instance operating " +
			"as a certificate authority for a client certificate.  The 'no' method " +
			"generates no certificate and the server is either run insecurely " +
			"(i.e. over http) or with externally provided certificates.",
	})
	f.BoolVar(&flags.BoolVar{
		Name:    "tls-disable-server",
		Default: false,
		EnvVar:  "TOOL_TLS_DISABLE_SERVER",
		Target:  &c.flagTlsDisableServer,
		Usage:   "Disable use of TLS certificates when running the vault server.",
	})

	// trusted root ca
	f.StringVar(&flags.StringVar{
		Name:    "root-ca-addr",
		Default: "",
		EnvVar:  "TOOL_ROOT_CA_VAULT_ADDR",
		Target:  &c.flagTrustedRootCaAddr,
		Usage:   "Address of the trusted root CA vault server.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "root-ca-token",
		Default: "",
		EnvVar:  "TOOL_ROOT_CA_VAULT_TOKEN",
		Target:  &c.flagTrustedRootCaToken,
		Usage:   "A wrapped vault token issued by the trusted root CA vault server.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "root-ca-pki-role",
		Default: "tlscert",
		EnvVar:  "TOOL_ROOT_CA_VAULT_PKI_ROLE",
		Target:  &c.flagTrustedRootCaRole,
		Usage:   "The name of the trusted root CA vault pki role.",
	})

	// 00-base.json
	f.StringVar(&flags.StringVar{
		Name:    "advertise-interface",
		Default: "eth0",
		EnvVar:  "TOOL_ADVERTISE_INTERFACE",
		Target:  &c.flagAdvertiseInterface,
		Usage:   "The advertise address is used to change the address that consul advertises to other nodes in the cluster.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "client-interface",
		Default: "lo",
		EnvVar:  "TOOL_CLIENT_INTERFACE",
		Target:  &c.flagClientInterface,
		Usage:   "The address to which Consul will bind client interfaces, including the HTTP and DNS servers.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "datacenter",
		Default: "docker",
		EnvVar:  "TOOL_DATACENTER",
		Target:  &c.flagDatacenter,
		Usage:   "This flag specifies the datacenter in which the agent is running.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "data-directory",
		Default: "/opt/consul/data",
		EnvVar:  "TOOL_DATA_DIR",
		Target:  &c.flagDataPath,
		Usage:   "This flag provides a data directory for the agent to store state.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "domain",
		Default: "consul.",
		EnvVar:  "TOOL_DOMAIN",
		Target:  &c.flagDomain,
		Usage:   "Specifies the domain in which consul responds to DNS queries.",
	})
	f.BoolVar(&flags.BoolVar{
		Name:    "enable-script-checks",
		Default: false,
		EnvVar:  "TOOL_ENABLE_SCRIPT_CHECKS",
		Target:  &c.flagEnableScriptChecks,
		Usage:   "This controls whether health checks that execute scripts are enabled on this agent, and defaults to false so operators must opt-in to allowing these.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "log-level",
		Default: "info",
		EnvVar:  "TOOL_LOG_LEVEL",
		Target:  &c.flagLogLevel,
		Usage:   "The level of logging to show after the Consul agent has started.",
	})
	f.IntVar(&flags.IntVar{
		Name:    "port-dns",
		Default: 8600,
		EnvVar:  "TOOL_PORT_DNS",
		Target:  &c.flagPortDns,
		Usage:   "This flag specifies the DNS port to listen on.",
	})
	f.IntVar(&flags.IntVar{
		Name:    "port-http",
		Default: 8500,
		EnvVar:  "TOOL_PORT_HTTP",
		Target:  &c.flagPortHttp,
		Usage:   "This flag specifies the HTTP port to listen on.",
	})
	f.IntVar(&flags.IntVar{
		Name:    "port-https",
		Default: -1,
		EnvVar:  "TOOL_PORT_HTTPS",
		Target:  &c.flagPortHttps,
		Usage:   "This flag specifies the HTTPS port to listen on.",
	})
	f.IntVar(&flags.IntVar{
		Name:    "port-self-lan",
		Default: 8301,
		EnvVar:  "TOOL_PORT_SELF_LAN",
		Target:  &c.flagPortSerfLan,
		Usage:   "The Serf LAN port.",
	})
	f.IntVar(&flags.IntVar{
		Name:    "port-self-wan",
		Default: 8302,
		EnvVar:  "TOOL_PORT_SELF_WAN",
		Target:  &c.flagPortSerfWan,
		Usage:   "The Serf WAN port.",
	})
	f.IntVar(&flags.IntVar{
		Name:    "port-server",
		Default: 8300,
		EnvVar:  "TOOL_PORT_SELF_WAN",
		Target:  &c.flagPortServer,
		Usage:   "The server RPC port.",
	})
	f.IntVar(&flags.IntVar{
		Name:    "raft-protocol",
		Default: 3,
		EnvVar:  "TOOL_RAFT_PROTOCOL",
		Target:  &c.flagRaftProtocol,
		Usage:   "This controls the internal version of the Raft consensus protocol used for server communications.",
	})

	// 01-gossip.json
	f.BoolVar(&flags.BoolVar{
		Name:    "gossip-encryption",
		Default: false,
		EnvVar:  "TOOL_GOSSIP_ENCRYPTION",
		Target:  &c.flagGossipEncryption,
		Usage:   "Specifies whether to encrypt Consul's network traffic.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "gossip-encrypt-key",
		Default: "",
		EnvVar:  "TOOL_GOSSIP_ENCRYPT_KEY",
		Target:  &c.flagGossipEncryptKey,
		Usage:   "Specifies the secret key to use for encryption of Consul network traffic.",
	})

	// 02-ssl.json
	f.BoolVar(&flags.BoolVar{
		Name:    "rpc-encryption",
		Default: false,
		EnvVar:  "TOOL_RPC_ENCRYPTION",
		Target:  &c.flagRpcEncryption,
		Usage:   "If set to true, Consul requires that all incoming connections make use of TLS and that the client provides a certificate signed by a Certificate Authority from the ca_file or ca_path.",
	})
	f.BoolVar(&flags.BoolVar{
		Name:    "rpc-verify-incoming",
		Default: false,
		EnvVar:  "TOOL_RPC_VERIFY_INCOMING",
		Target:  &c.flagRpcVerifyIncoming,
		Usage:   "If set to true, Consul requires that all incoming connections make use of TLS and that the client provides a certificate signed by a Certificate Authority from the ca_file or ca_path.",
	})
	f.BoolVar(&flags.BoolVar{
		Name:    "rpc-verify-outgoing",
		Default: false,
		EnvVar:  "TOOL_RPC_VERIFY_OUTGOING",
		Target:  &c.flagRpcVerifyOutgoing,
		Usage:   "If set to true, Consul requires that all outgoing connections make use of TLS and that the server provides a certificate that is signed by a Certificate Authority from the ca_file or ca_path.",
	})

	// 03-server.json
	f.StringVar(&flags.StringVar{
		Name:    "entrypoint-mode",
		Default: "server",
		EnvVar:  "TOOL_ENTRYPOINT_MODE",
		Target:  &c.flagEntrypointMode,
		Usage:   "Specifies whether consul will be configured as a 'client' or 'server'.",
	})

	// 04-cluster.json
	f.StringVar(&flags.StringVar{
		Name:    "server-name",
		Default: "",
		EnvVar:  "TOOL_SERVER_NAME",
		Target:  &c.flagServerName,
		Usage:   "Specifies the server-name.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "server-list",
		Default: "",
		EnvVar:  "TOOL_SERVER_LIST",
		Target:  &c.flagServerList,
		Usage:   "Specifies the comma-separated list server-names in the cluster.",
	})

	// 05-dns.json
	f.StringVar(&flags.StringVar{
		Name:    "recursor-list",
		Default: "",
		EnvVar:  "TOOL_RECURSOR_LIST",
		Target:  &c.flagRecursorList,
		Usage:   "This flag provides a comma-separated addresses of upstream DNS servers that are used to recursively resolve queries if they are not inside the service domain for Consul.",
	})

	// 06-telemetry.json
	f.BoolVar(&flags.BoolVar{
		Name:    "telemetry-enable",
		Default: false,
		EnvVar:  "TOOL_TELEMETRY_ENABLE",
		Target:  &c.flagTelemetryEnable,
		Usage:   "This flag specifies whether Consul publishes its runtime telemetry.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "dogstatsd-addr",
		Default: "",
		EnvVar:  "TOOL_DOGSTATSD_ADDR",
		Target:  &c.flagDogstatsdAddr,
		Usage:   "This provides the address of a DogStatsD instance in the format host:port.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "dogstatsd-tags",
		Default: "",
		EnvVar:  "TOOL_DOGSTATSD_TAGS",
		Target:  &c.flagDogstatsdTags,
		Usage:   "This provides a list of global tags that will be added to all telemetry packets sent to DogStatsD.",
	})
	f.StringVar(&flags.StringVar{
		Name:    "statsd-addr",
		Default: "",
		EnvVar:  "TOOL_STATSD_TAGS",
		Target:  &c.flagStatsdAddr,
		Usage:   "This provides the address of a statsd instance in the format host:port.",
	})

	return set
}

func (c *EntrypointCommand) AutocompleteArgs() complete.Predictor {
	return complete.PredictNothing
}

func (c *EntrypointCommand) AutocompleteFlags() complete.Flags {
	return c.Flags().Completions()
}

func (c *EntrypointCommand) Run(args []string) int {
	f := c.Flags()

	if err := f.Parse(args); err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	directories := []string{
		c.flagConfigurationPath,
		filepath.Dir(c.flagTlsCACertificate),
		filepath.Dir(c.flagTlsCertificate),
		filepath.Dir(c.flagTlsPrivateKey),
	}

	if err := createDirectories(directories...); err != nil {
		c.UI.Error(errors.Wrap(err, "could not create directories").Error())
		return 1
	}

	switch c.flagTlsProvision {
	case "vault":
		if err := c.provisionCertificateUsingVault(); err != nil {
			c.UI.Error(errors.Wrap(err, "error requesting certificate from vault").Error())
			return 1
		}
	case "no":
		break
	default:
		c.UI.Error(errors.New("unknown certificate provisioning option").Error())
		return 1
	}

	advertise, err := lookupInterfaceIP(c.flagAdvertiseInterface)
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	var client = "0.0.0.0"
	if c.flagClientInterface != "" {
		ip, err := lookupInterfaceIP(c.flagClientInterface)
		if err != nil {
			c.UI.Error(err.Error())
			return 1
		}
		client = ip.String()
	}

	hostname, _, _, err := networkInformation()
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	// 00-base.json
	baseData := make(map[string]interface{})
	baseData["advertise_addr"] = advertise.String()
	baseData["client_addr"] = client
	baseData["datacenter"] = c.flagDatacenter
	baseData["data_dir"] = c.flagDataPath
	baseData["domain"] = c.flagDomain
	baseData["enable_script_checks"] = c.flagEnableScriptChecks
	baseData["log_level"] = c.flagLogLevel
	baseData["pid_file"] = "/var/run/consul/consul.pid"

	portsData := make(map[string]interface{})
	portsData["dns"] = c.flagPortDns
	portsData["http"] = c.flagPortHttp
	portsData["https"] = c.flagPortHttps
	portsData["serf_lan"] = c.flagPortSerfLan
	portsData["serf_wan"] = c.flagPortSerfWan
	portsData["server"] = c.flagPortServer

	baseData["ports"] = portsData
	baseData["raft_protocol"] = c.flagRaftProtocol

	err = writeConfigurationFile(filepath.Join(c.flagConfigurationPath, "00-base.json"), baseData)
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	// 01-gossip.json
	if c.flagGossipEncryption {
		// TODO: vault read -field symmetric_key secret/consul/gossip
		if len(c.flagGossipEncryptKey) != 24 {
			c.UI.Error("Gossip encryption key must be exactly 16 bytes BASE64 encoded (i.e. 24 characters)")
			return 1
		}

		gossipData := make(map[string]interface{})
		gossipData["encrypt"] = c.flagGossipEncryptKey

		err = writeConfigurationFile(filepath.Join(c.flagConfigurationPath, "01-gossip.json"), gossipData)
		if err != nil {
			c.UI.Error(err.Error())
			return 1
		}
	}

	// 02-ssl.json
	if c.flagRpcEncryption {
		// TODO: assert certificates actually exist
		rpcData := make(map[string]interface{})
		rpcData["verify_incoming"] = c.flagRpcVerifyIncoming
		rpcData["verify_outgoing"] = c.flagRpcVerifyOutgoing
		rpcData["ca_file"] = c.flagTlsCACertificate
		rpcData["cert_file"] = c.flagTlsCertificate
		rpcData["key_file"] = c.flagTlsPrivateKey
		rpcData["enable_agent_tls_for_checks"] = true

		err = writeConfigurationFile(filepath.Join(c.flagConfigurationPath, "02-ssl.json"), rpcData)
		if err != nil {
			c.UI.Error(err.Error())
			return 1
		}
	}

	// 03-server.json
	serverData := make(map[string]interface{})
	serverData["node_name"] = hostname

	if c.flagEntrypointMode == "server" {
		serverData["server"] = true
	} else if c.flagEntrypointMode == "client" {
		serverData["server"] = false
	} else {
		c.UI.Error(fmt.Sprintf("entrypoint mode neither 'client' nor 'server': %s", c.flagEntrypointMode))
		return 1
	}

	err = writeConfigurationFile(filepath.Join(c.flagConfigurationPath, "03-server.json"), serverData)
	if err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	// 04-cluster.json
	if len(c.flagServerName) > 0 && len(c.flagServerList) > 0 {
		serverList := strings.Split(c.flagServerList, ",")
		serverName := c.flagServerName

		clusterData := make(map[string]interface{})
		if c.flagEntrypointMode == "server" {
			var index = -1
			for i, server := range serverList {
				if server == serverName {
					index = i
					break
				}
			}
			if len(serverList) == 1 {
				clusterData["bootstrap"] = true
			} else if index == 0 {
				clusterData["retry_join"] = serverList
			} else {
				clusterData["bootstrap_expect"] = len(serverList)
			}
		} else {
			clusterData["retry_join"] = serverList
		}

		err = writeConfigurationFile(filepath.Join(c.flagConfigurationPath, "04-cluster.json"), clusterData)
		if err != nil {
			c.UI.Error(err.Error())
			return 1
		}
	}

	// 05-dns.json
	if len(c.flagRecursorList) > 0 {
		recursorList := strings.Split(c.flagRecursorList, ",")
		if len(recursorList) > 0 {
			dnsData := make(map[string]interface{})
			dnsData["recursors"] = recursorList

			err = writeConfigurationFile(filepath.Join(c.flagConfigurationPath, "05-dns.json"), dnsData)
			if err != nil {
				c.UI.Error(err.Error())
				return 1
			}
		}
	}

	// 06-telemetry.json
	if c.flagTelemetryEnable {
		telemetryData := make(map[string]interface{})

		if len(c.flagDogstatsdAddr) > 0 {
			dogstatsdData := make(map[string]interface{})
			dogstatsdData["dogstatsd_addr"] = c.flagDogstatsdAddr
			dogstatsdData["dogstatsd_tags"] = c.flagDogstatsdTags
			telemetryData["telemetry"] = dogstatsdData
		} else if len(c.flagDogstatsdAddr) > 0 {
			statsdData := make(map[string]interface{})
			statsdData["statsd_address"] = c.flagStatsdAddr
			telemetryData["telemetry"] = statsdData
		} else {
			c.UI.Error("telemetry enabled but neither dogstatsd nor statsd servers provided")
			return 1
		}

		err = writeConfigurationFile(filepath.Join(c.flagConfigurationPath, "06-telemetry.json"), telemetryData)
		if err != nil {
			c.UI.Error(err.Error())
			return 1
		}
	}

	// 07-acls.json
	if c.flagAclEnable {
		aclsData := make(map[string]interface{})
		aclsData["acl_datacenter"] = c.flagDatacenter
		aclsData["acl_default_policy"] = "deny"
		aclsData["acl_enable_key_list_policy"] = "true"

		err = writeConfigurationFile(filepath.Join(c.flagConfigurationPath, "07-acls.json"), aclsData)
		if err != nil {
			c.UI.Error(err.Error())
			return 1
		}
	}

	consul, err := exec.LookPath("consul")
	if err != nil {
		c.UI.Error(errors.Wrap(err, "consul executable not in path").Error())
		return 1
	}

	consulArgs := []string{"consul", "agent", "-data-dir=" + c.flagDataPath, "-config-dir=" + c.flagConfigurationPath}
	consulEnv := os.Environ()

	c.UI.Info(fmt.Sprintf("consul path: %v", consul))
	c.UI.Info(fmt.Sprintf("consul args: %v", strings.Join(consulArgs, " ")))

	err = syscall.Exec(consul, consulArgs, consulEnv)
	if err != nil {
		c.UI.Error(errors.Wrap(err, "failed to exec consul").Error())
		return 1
	}

	return 0 // but really not ... because the exec succeeded
}

func writeConfigurationFile(path string, data map[string]interface{}) error {
	file, err := os.Create(path)
	if err != nil {
		return errors.Wrapf(err, "error opening '%s'", path)
	}
	defer file.Close()

	bytes, err := json.Marshal(data)
	if err != nil {
		return errors.Wrapf(err, "error forming '%s'", path)
	}

	_, err = file.Write(bytes)
	if err != nil {
		return errors.Wrapf(err, "error writing '%s'", path)
	}
	return nil
}

func (c *EntrypointCommand) provisionCertificateUsingVault() error {
	c.UI.Info("Provisioning TLS certificates using Vault.")
	c.UI.Info(fmt.Sprintf("address: %s", c.flagTrustedRootCaAddr))
	c.UI.Info(fmt.Sprintf("token:   %s", c.flagTrustedRootCaToken))
	c.UI.Info(fmt.Sprintf("role:    %s", c.flagTrustedRootCaRole))
	c.UI.Info(fmt.Sprintf("ca-cert: %s", c.flagTlsCACertificate))

	hostname, ips, names, err := networkInformation()
	if err != nil {
		return errors.Wrap(err, "failed to get network information")
	}

	config := &api.Config{Address: c.flagTrustedRootCaAddr}
	if _, err := os.Stat(c.flagTlsCACertificate); err != nil {
		config.ConfigureTLS(&api.TLSConfig{Insecure: true})
	} else {
		config.ConfigureTLS(&api.TLSConfig{CACert: c.flagTlsCACertificate})
	}

	client, err := api.NewClient(config)
	if err != nil {
		return errors.Wrap(err, "error creating vault api client")
	}

	token, err := client.Logical().Unwrap(c.flagTrustedRootCaToken)
	if err != nil {
		return errors.Wrap(err, "error unwrapping token")
	}

	c.flagTrustedRootCaToken = token.Auth.ClientToken

	client.SetToken(c.flagTrustedRootCaToken)

	cn := hostname
	var altNames []string
	for i, name := range names {
		name = strings.TrimSuffix(name, ".")
		if i == 0 {
			cn = name
		}
		altNames = append(altNames, name)
	}

	var ipSans []string
	for _, ip := range ips {
		ipSans = append(ipSans, ip.String())
	}

	options := make(map[string]interface{})
	options["common_name"] = cn
	options["alt_names"] = strings.Join(altNames, ",")
	options["ttl"] = "4380h"
	options["ip_sans"] = strings.Join(ipSans, ",")
	options["format"] = "pem"

	secret, err := client.Logical().Write(fmt.Sprintf("pki/issue/%s", c.flagTrustedRootCaRole), options)
	if err != nil {
		return errors.Wrap(err, "error requesting pki")
	}

	caCertificateFile, err := os.Create(c.flagTlsCACertificate)
	if err != nil {
		return errors.Wrap(err, "failed to open ca certificate file for writing")
	}
	defer caCertificateFile.Close()

	certificateFile, err := os.Create(c.flagTlsCertificate)
	if err != nil {
		return errors.Wrap(err, "failed to open certificate file for writing")
	}
	defer certificateFile.Close()

	privateKeyFile, err := os.OpenFile(c.flagTlsPrivateKey, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		return errors.Wrap(err, "failed to open private key file for writing")
	}
	defer privateKeyFile.Close()

	var n int

	certificateWriter := bufio.NewWriter(certificateFile)
	n, err = fmt.Fprint(certificateWriter, secret.Data["certificate"])
	if n == 0 || err != nil {
		return errors.Wrap(err, "error writing certificate [certificate]")
	}
	n, err = certificateWriter.WriteString("\n")
	if n == 0 || err != nil {
		return errors.Wrap(err, "error writing certificate [newline separator]")
	}
	n, err = fmt.Fprint(certificateWriter, secret.Data["issuing_ca"])
	if n == 0 || err != nil {
		return errors.Wrap(err, "error writing certificate [issuing_ca]")
	}
	certificateWriter.Flush()

	privateKeyWriter := bufio.NewWriter(privateKeyFile)
	n, err = fmt.Fprint(privateKeyWriter, secret.Data["private_key"])
	if n == 0 || err != nil {
		return errors.Wrap(err, "error writing private_key")
	}
	privateKeyWriter.Flush()

	caCertificateWriter := bufio.NewWriter(caCertificateFile)
	n, err = fmt.Fprint(caCertificateWriter, secret.Data["issuing_ca"])
	if n == 0 || err != nil {
		return errors.Wrap(err, "error writing ca certificate")
	}
	caCertificateWriter.Flush()

	return nil
}

func createDirectories(paths ...string) error {
	for _, path := range paths {
		dir := filepath.Dir(path)
		if _, err := os.Stat(dir); os.IsNotExist(err) {
			if err := os.MkdirAll(dir, os.ModeDir); err != nil {
				return err
			}
		}
	}
	return nil
}
