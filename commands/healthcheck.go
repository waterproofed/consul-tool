// Copyright © 2018 Simon Wenmouth <simon-wenmouth@users.noreply.github.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package commands

import (
	"strings"

		"github.com/posener/complete"
	"github.com/pkg/errors"
	"github.com/simon-wenmouth/consul-tool/flags"
	"github.com/hashicorp/consul/api"
	"fmt"
)

type HealthCheckCommand struct {
	*BaseCommand
}

func (c *HealthCheckCommand) Help() string {
	helpText := `
Usage: vault-tool healthcheck [options]

      The healthcheck command is used to report on the health of a running
      vault instance.

` + c.Flags().Help()

	return strings.TrimSpace(helpText)
}

func (c *HealthCheckCommand) Synopsis() string {
	return "Docker HEALTHCHECK"
}

func (c *HealthCheckCommand) Flags() *flags.FlagSets {
	set := c.flagSet(FlagSetTLS | FlagSetClientConsul)
	return set
}

func (c *HealthCheckCommand) AutocompleteArgs() complete.Predictor {
	return complete.PredictNothing
}

func (c *HealthCheckCommand) AutocompleteFlags() complete.Flags {
	return c.Flags().Completions()
}

func (c *HealthCheckCommand) Run(args []string) int {
	f := c.Flags()

	if err := f.Parse(args); err != nil {
		c.UI.Error(err.Error())
		return 1
	}

	client, err := c.consul()
	if err != nil {
		c.UI.Error(errors.Wrap(err, "Error creating Consul client.").Error())
		return 1
	}

	hostname, _, _, err := networkInformation()
	if err != nil {
		c.UI.Error(errors.Wrap(err, "Error getting network information.").Error())
		return 1
	}

	checks, _, err := client.Health().Node(hostname, &api.QueryOptions{})
	if err != nil {
		c.UI.Error(errors.Wrap(err, "Error getting health information.").Error())
		return 1
	}

	var healthy bool = false
	for _, check := range checks {
		if check.CheckID == "serfHealth" {
			if check.Status == "passing" {
				healthy = true
			} else {
				c.UI.Warn(fmt.Sprintf("Serf Health: %v", check.Status))
			}
		}
	}

	if !healthy {
		c.UI.Warn("Consul is unhealthy.")
		return 1
	}

	c.UI.Info("Consul is healthy.")
	return 0
}
