FROM centos:7.4.1708 as build

ARG KEYSERVER=ha.pool.sks-keyservers.net
ARG CONSUL_VERSION=1.2.2
ARG CONSUL_ZIP_URL=https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip
ARG CONSUL_SIG_URL=https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_SHA256SUMS.sig
ARG CONSUL_SHA_URL=https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_SHA256SUMS

RUN mkdir --parents "/opt/consul"/{bin,data} "/opt/consul"/config/keys

WORKDIR /opt/consul

RUN set -x \
    && yum -y install unzip \
    && gpg --keyserver "${KEYSERVER}" --recv-keys 51852D87348FFC4C \
    && curl -fSL "${CONSUL_ZIP_URL}" -o "consul_${CONSUL_VERSION}_linux_amd64.zip" \
    && curl -fSL "${CONSUL_SHA_URL}" -o "consul_${CONSUL_VERSION}.sha" \
    && curl -fSL "${CONSUL_SIG_URL}" -o "consul_${CONSUL_VERSION}.sha.sig" \
    && gpg --verify "consul_${CONSUL_VERSION}.sha.sig" "consul_${CONSUL_VERSION}.sha" \
    && grep "consul_${CONSUL_VERSION}_linux_amd64.zip" "consul_${CONSUL_VERSION}.sha" > "consul_${CONSUL_VERSION}_linux_amd64.sha" \
    && sha256sum --check "consul_${CONSUL_VERSION}_linux_amd64.sha" \
    && unzip "consul_${CONSUL_VERSION}_linux_amd64.zip" -d "/opt/consul/bin/" \
    && rm "consul_${CONSUL_VERSION}_linux_amd64.zip" \
    && rm "consul_${CONSUL_VERSION}_linux_amd64.sha" \
    && rm "consul_${CONSUL_VERSION}.sha" \
    && rm "consul_${CONSUL_VERSION}.sha.sig"

FROM busybox:glibc

LABEL name="Docker Container"
LABEL vendor="Hashicorp, Inc",
LABEL license="Mozilla Public License, version 2.0"
LABEL maintainer="simon-wenmouth@users.noreply.github.com"

ARG CONSUL_UID=102
ARG CONSUL_GID=102
ARG CONSUL_LOGIN=consul

ENV CONSUL_HOME=/opt/consul \
    CONSUL_VERSION=${CONSUL_VERSION}

ENV PATH=${PATH}:${CONSUL_HOME}/bin

RUN set -x && mkdir --parents /var/log/consul /var/run/consul /opt

COPY --from=build /opt/ /opt/

COPY --from=build /etc/pki/ /etc/pki/

COPY --from=build /etc/ssl/certs/ /etc/ssl/certs/

COPY pkg/linux_amd64/consul-tool /opt/consul/bin/

RUN set -x \
    && addgroup -g "${CONSUL_GID}" -S "${CONSUL_LOGIN}" \
    && adduser -S -s /bin/sh -u "${CONSUL_UID}" -G "${CONSUL_LOGIN}" "${CONSUL_LOGIN}" \
    && chown -R "${CONSUL_UID}":"${CONSUL_GID}" /opt/consul /var/log/consul /var/run/consul \
    && chmod a+x /opt/consul/bin/*

USER ${CONSUL_UID}

WORKDIR /opt/consul/

VOLUME /opt/consul/data/

VOLUME /var/log/consul/

# Server RPC is used for communication between Consul clients and servers for internal
# request forwarding.
EXPOSE 8300

# Serf LAN and WAN (WAN is used only by Consul servers) are used for gossip between
# Consul agents. LAN is within the datacenter and WAN is between just the Consul
# servers in all datacenters.
EXPOSE 8301 8301/udp 8302 8302/udp

# CLI, HTTP, and DNS (both TCP and UDP) are the primary interfaces that applications
# use to interact with Consul.
EXPOSE 8400 8500 8600 8600/udp

HEALTHCHECK CMD /opt/consul/bin/consul-tool healthcheck

ENTRYPOINT ["/opt/consul/bin/consul-tool"]

CMD ["entrypoint"]
