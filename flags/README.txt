these files were authored by the awesome folks at hashicorp and come from
the `github.com/hashicorp/vault` repository. see:

    https://github.com/hashicorp/vault/blob/v0.10.1/command/base.go
    https://github.com/hashicorp/vault/blob/v0.10.1/command/base_flags.go

when the `command` package is imported it downloads all dependencies
of vault which is unnecessary. this has the unfortunate side effect
of breaking `dep ensure`.  which is why, for now, this project is
built using `govendor` and not `dep`.

i'd like to get these files into the `vendor` folder at some point. or
have them exist in `github.com/mitchellh/cli which would be even nicer.
