#!/usr/bin/env bash

set -u
set -o pipefail

SCRIPTS_DIR="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd -P)"
ROOT_DIR=$(cd "${SCRIPTS_DIR}" && git rev-parse --show-toplevel)

pushd "${ROOT_DIR}"

GIT_REPOSITORY=$(git ls-remote --get-url)
GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
GIT_COMMIT_HASH=$(git rev-parse HEAD)
BUILD_TIME=$(date -u +'%Y-%m-%dT%H-%M-%SZ')

IMPORT_PATH="github.com/simon-wenmouth/consul-tool/cli"
LD_FLAGS="-X ${IMPORT_PATH}.GitRepository=${GIT_REPOSITORY} -X ${IMPORT_PATH}.GitBranch=${GIT_BRANCH} -X ${IMPORT_PATH}.GitCommitHash=${GIT_COMMIT_HASH} -X ${IMPORT_PATH}.BuildTime=${BUILD_TIME}"
GC_FLAGS=${GC_FLAGS:-}
GO_TAGS=${GO_TAGS:-}

XC_ARCH=${XC_ARCH:-"386 amd64"}
XC_OS=${XC_OS:-linux darwin windows}
XC_OSARCH=${XC_OSARCH:-"linux/386 linux/amd64 linux/arm linux/arm64 darwin/386 darwin/amd64 windows/386 windows/amd64"}

DEV_BUILD=${DEV_BUILD:-false}
if [[ "${DEV_BUILD}" == "true" ]]; then
    XC_OS=$(go env GOOS)
    XC_ARCH=$(go env GOARCH)
    XC_OSARCH=$(go env GOOS)/$(go env GOARCH)
fi

for dir_name in bin pkg; do
    if [[ -d "${dir_name}" ]]; then
        rm -Rf "${dir_name}"
    fi
    mkdir "${dir_name}"
done

gox -osarch="${XC_OSARCH}" \
    -gcflags "${GC_FLAGS}" \
    -ldflags "${LD_FLAGS}" \
    -output "pkg/{{.OS}}_{{.Arch}}/consul-tool" \
    -tags="${GO_TAGS}" \
    .

GOBIN=${GOBIN:-$(go env GOPATH)/bin}
DEV_BINARY="./pkg/$(go env GOOS)_$(go env GOARCH)/consul-tool"
if [[ -f "${DEV_BINARY}" ]]; then
    cp -v "${DEV_BINARY}" bin/
    cp -v "${DEV_BINARY}" "${GOBIN}"
fi

popd
