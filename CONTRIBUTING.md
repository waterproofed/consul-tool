# Contributing

Thank you for your interest in contributing to consul-tool!

When contributing to this repository, please first discuss the change
you wish to make via issue, email, or any other method with the owners
of this repository before making a change. 

Please note we have a code of conduct, please follow it in all your
interactions with the project.

## Pull Requests

Pull requests are the primary mechanism used to change consul-tool.
GitHub itself has some [great documentation][about-pull-requests]
on using the Pull Request feature. We use the "fork and pull" model
[described here][development-models], where contributors push changes
to their personal fork and create pull requests to bring those
changes into the source repository.

[about-pull-requests]: https://help.github.com/articles/about-pull-requests/
[development-models]: https://help.github.com/articles/about-collaborative-development-models/

Please make pull requests against the `master` branch.

## Guidelines

Please ensure your pull request adheres to the following guidelines:

- Search previous suggestions before making a new one, as yours may be a duplicate.
- Make an individual pull request for each suggestion.
- Check your spelling and grammar.
- Make sure you have run `go fmt` on altered files.
- The pull request and commit should have a useful title.
