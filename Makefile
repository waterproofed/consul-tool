
.PHONY: bin dev get fmt test vet

default: dev

# build for all architectures
bin:
	@DEV_BUILD=false sh -c "'$(CURDIR)/scripts/build.sh'"

# build for development architecture
dev:
	@DEV_BUILD=true sh -c "'$(CURDIR)/scripts/build.sh'"

# fetch the build tools and vendor files
get:
	go get -u github.com/mitchellh/gox
	go get -u github.com/kardianos/govendor
	govendor sync

# reformat the source code
fmt:
	go fmt $(go list ./... | grep -v /vendor/)

# run the unit tests
test:
	go test $(go list ./... | grep -v /vendor/)

# lint the code
vet:
	go vet $(go list ./... | grep -v /vendor/)
